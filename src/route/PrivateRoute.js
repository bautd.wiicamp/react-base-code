/* eslint-disable react/jsx-props-no-spreading */
import PropTypes from "prop-types";
import { Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

import getLocationFullPath from "helpers/getLocationFullPath";

function PrivateRoute({ component: Component, ...params }) {
  const uuid = useSelector((state) => state.auth.uuid);

  if (!uuid) {
    const redirect = getLocationFullPath();
    return <Redirect to={`/login?redirect=${redirect}`} />;
  }

  return (
    <Route
      render={(otherProps) => {
        const newParams = { ...otherProps, ...params };

        return <Component {...newParams} />;
      }}
    />
  );
}

PrivateRoute.propTypes = {
  component: PropTypes.elementType.isRequired,
};

PrivateRoute.defaultProps = {};

export default PrivateRoute;
