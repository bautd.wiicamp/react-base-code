/* eslint-disable react/jsx-props-no-spreading */
import PropTypes from "prop-types";
import { Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

function PublishRoute({ isForGuestOnly, component: Component, ...params }) {
  const uuid = useSelector((state) => state.auth.uuid);

  if (isForGuestOnly && uuid) {
    return <Redirect to="/homepage" />;
  }

  return (
    <Route
      render={(otherProps) => {
        const newParams = { ...otherProps, ...params };

        return <Component {...newParams} />;
      }}
    />
  );
}

PublishRoute.propTypes = {
  isForGuestOnly: PropTypes.bool,
  component: PropTypes.elementType.isRequired,
};

PublishRoute.defaultProps = {
  isForGuestOnly: false,
};

export default PublishRoute;
