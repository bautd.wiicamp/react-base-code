import { Link } from "react-router-dom";

const WelcomePage = () => {
  return (
    <div>
      <div>this is welcome page</div>
      <Link to="/homepage">Click here to Homepage</Link>
    </div>
  );
};

export default WelcomePage;
