import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, Redirect } from "react-router-dom";

import getObjectOfURLQueries from "helpers/getObjectOfURLQueries";
import { setAuthentication } from "reduxStore/rootReducers/UserReducer/actions";

const LoginPage = ({ ...params }) => {
  const { redirect } = getObjectOfURLQueries(params.location.search);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const uuid = useSelector((state) => state.auth.uuid);
  const dispatch = useDispatch();
  let watching = null;

  const login = () => {
    clearTimeout(watching);
    watching = setTimeout(() => {
      dispatch(setAuthentication.request({ username, password }));
    }, 300);
  };

  if (uuid) {
    if (redirect) {
      return <Redirect to={redirect} />;
    }

    return <Redirect to="/no-redirect-route" />;
  }
  return (
    <div>
      <div>This is login page</div>
      <div>
        <input
          placeholder="Username"
          onChange={(e) => setUsername(e.target.value)}
        />
        <input
          type="password"
          placeholder="Password"
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>
      <button type="submit" onClick={login}>
        Login
      </button>
      <Link to="/">Back to welcome page</Link>
    </div>
  );
};

export default LoginPage;
