/* eslint-disable no-unused-vars */
import { takeLatest, call, put, all } from "redux-saga/effects";

import { SET_AUTHENTICATION } from "./constants";
import { setAuthentication } from "./actions";

function* setAuthenticationTask({ payload }) {
  try {
    yield put(setAuthentication.success({ userInfo: payload }));
  } catch (error) {
    yield put(setAuthentication.failure(error));
  }
}

export default [takeLatest(SET_AUTHENTICATION.REQUEST, setAuthenticationTask)];
