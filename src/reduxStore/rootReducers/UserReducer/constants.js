import { createFetchActionTypes } from "reduxStore/utils";

const CONTEXT = "AUTHENTICATION";

export const SET_AUTHENTICATION = createFetchActionTypes(CONTEXT);

export const RESET_STATE = `${CONTEXT}/RESET_STATE`;
