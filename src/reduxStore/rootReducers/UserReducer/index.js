import { SET_AUTHENTICATION, RESET_STATE } from "./constants";

const initialState = {
  userInfo: {},
  uuid: "",
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_AUTHENTICATION.SUCCESS: {
      return {
        ...state,
        ...payload,
        uuid: "12312",
      };
    }

    case RESET_STATE: {
      return initialState;
    }

    default:
      return state;
  }
};
